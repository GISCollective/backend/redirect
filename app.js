const Hapi = require("@hapi/hapi");
const querystring = require('querystring');

const redirectHandler = function (request, h) {
  let query = querystring.encode(request.query);
  let path = "https://greenmap.org" + request.path;

  if(query) {
    path += "?" + query;
  }

  return h.redirect(path);
};

const init = async () => {
  const server = Hapi.server({
    port: 3000,
    host: '0.0.0.0',
  });

  server.route({
    method: 'GET',
    path: '/{any*}',
    handler: redirectHandler
});

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
