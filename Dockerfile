FROM node:22-alpine

LABEL mantainer="Szabo Bogdan <contact@szabobogdan.com>"

WORKDIR /home/node/app
USER root

# Copy the app files
COPY . .

RUN npm install

EXPOSE 3000

CMD [ "node", "app.js" ]
